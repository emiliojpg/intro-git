doc=gpul-introgit
SHELL=/bin/bash

default: pdf

pdf: $(doc).pdf
svg: $(doc)_1.svg

all: pdf svg

SRC=$(wildcard *.tex *.sty)

%.pdf: %.tex $(SRC)
	xelatex '\newcommand\queformato{0}\input{$<}'
	xelatex '\newcommand\queformato{0}\input{$<}'

%_1.svg: %.dvi $(SRC)
	dvisvgm --bbox=papersize --font-format=woff2 --zoom=-1 --page=- --output=%f-%0p.svg $<

%.dvi: %.tex
	dvilualatex $<
	dvilualatex $<

clean:
	rm -f *.{out,aux,toc,log,bbl,blg,bak,snm,nav} *\~

mrproper: clean
	rm -f *.{ps,dvi,pdf,vrb,xdv,svg}
